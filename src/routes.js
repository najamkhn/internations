import Users from './components/Users.vue'
import UserEdit from './components/UserEdit.vue'
import UserAdd from './components/UserAdd.vue'

import Groups from './components/Groups.vue'
import GroupAdd from './components/GroupAdd.vue'

import All from './components/All.vue'
import App from './components/App.vue'

export default {
    '/': {
        component: App
    },

    '/users': {
        component: Users
    },

    '/user/edit/:id': {
        name: 'user_edit',
        component: UserEdit
    },

    '/user/add': {
        name: 'user_add',
        component: UserAdd
    },

    '/groups': {
        component: Groups
    },

    '/group/add': {
        name: 'group_add',
        component: GroupAdd
    },

    '/all': {
        component: All
    }
}
