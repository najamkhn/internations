import ForerunnerDB from 'forerunnerdb'

const fdb = new ForerunnerDB(),
      db = fdb.db("myDatabaseName"),
      usersCollection = db.collection('users'),
      groupsCollection = db.collection('groups')

usersCollection.insert([{
  "_id": 3,
  "isActive": false,
  "first_name": "Kari",
  "last_name": "Cook",
  "email": "karicook@kaggle.com",
  "groups": [1],
  "password": "57823ae9863fea8f0540ff4b"
}, {
  "_id": 2,
  "isActive": true,
  "first_name": "Weaver",
  "last_name": "Everett",
  "email": "weavereverett@kaggle.com",
  "groups": [1, 3],
  "password": "57823ae9cf02b39407fd4181"
}, {
  "_id": 1,
  "isActive": false,
  "first_name": "Mcgee",
  "last_name": "Dorsey",
  "email": "mcgeedorsey@kaggle.com",
  "groups": [1, 2],
  "password": "57823ae94932ec1da58f983f"
}, {
  "_id": 4,
  "isActive": true,
  "first_name": "Little",
  "last_name": "Contreras",
  "email": "littlecontreras@kaggle.com",
  "groups": [1, 3],
  "password": "57823ae9e8edf999998b15cf"
}, {
  "_id": 6,
  "isActive": false,
  "first_name": "Baldwin",
  "last_name": "Phelps",
  "email": "baldwinphelps@kaggle.com",
  "groups": [1, 2],
  "password": "57823ae9a3b682a7b906311d"
}], () => true);

groupsCollection.insert([{
    "_id": 1,
    "name": "Parcoe",
    "isActive": true,
    "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi commodi, velit corporis non, obcaecati odio dolorum consequatur illum fugit eum reiciendis a facere necessitatibus hic possimus saepe voluptate. Eligendi, rerum."

  }, {
    "_id": 2,
    "name": "Handshake",
    "isActive": true,
    "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi commodi, velit corporis non, obcaecati odio dolorum consequatur illum fugit eum reiciendis a facere necessitatibus hic possimus saepe voluptate. Eligendi, rerum."
  }, {
    "_id": 3,
    "name": "Insectus",
    "isActive": true,
    "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi commodi, velit corporis non, obcaecati odio dolorum consequatur illum fugit eum reiciendis a facere necessitatibus hic possimus saepe voluptate. Eligendi, rerum."
  }
], () => true);

export default {
  groups() {
    return groupsCollection.find()
  },

  users() {
    return usersCollection.find()
  },

  collections: {
    users: usersCollection,
    groups: groupsCollection
  }
}
