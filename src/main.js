import Vue from 'vue'
import VueRouter from 'vue-router'
import VueFire from 'vuefire'
import Routes from './routes'
import App from './components/App.vue'

Vue.use(VueRouter)
Vue.use(VueFire)

const router = new VueRouter({
  history: true,
  transitionOnLoad: true
})

router.map(Routes);
router.start(App, '#app')
