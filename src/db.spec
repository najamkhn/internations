// Users
[
  {
    'repeat(5, 5)': {
      _id: '{{index()}}',
      first_name: '{{firstName()}}',
      last_name: '{{surname()}}',
      company: '{{company()}}',
      groups: '{{ }}'
      email: function (tags) {
        return (this.first_name + '.' + this.last_name + '@' + this.company + tags.domainZone()).toLowerCase();
     }
    }
  }
]
