# Stories Covered
- &#10004; I can see a list of existing users
- &#10004; I can see a list of existing groups
- &#10004; I can create users
- &#10004; I can create groups
- &#10004; I can assign users to a group they aren’t already part of
- ~~&#10004; I can remove users from a group~~
- ~~&#10004; I can delete users~~

## Technologies Used
- Webpack for packaging and resource building
- Webpack Dev Server for serving files
- Vue.js for all the reactive elements
- VueRouter for routing
- MDL (Material Design Lite) for CSS and JS boilerplate
- MDL Icon Font
- ForerunnerDB
- Babel for ES6


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

### NOTE: Some of the things among the list may not work because of some last minute refactoring.
